<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89620420-1"></script>
    <meta charset="utf-8">
    <title>Pierre HERAUD | Développeur WEB</title>

    <link rel="icon" href="img/favicon.ico"/>

    <meta itemprop="name" content="Pierre HERAUD | Développeur WEB">
    <meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Pierre HERAUD"/>
    <meta name="google-site-verification" content="PI1Htp_GiVC-P8CzGmgBNntFebSsM0freH0rcNMdpS4"/>
    <meta name="description"
          content="Pierre HERAUD, âgé de 25 ans, étudiant en quatrième année d'informatique à Keyce Academy et en alternance en tant que Développeur WEB dans l'agence de communication Suncha."/>
    <meta itemprop="description"
          content="Pierre HERAUD, âgé de 25 ans, étudiant en quatrième année d'informatique à Keyce Academy et en alternance en tant que Développeur WEB dans l'agence de communication Suncha.">
    <meta name="keywords"
          content="Pierre HERAUD, informatique, Keyce academy, Montpellier, Développeur web, wordpress, site web, informaticien, stage, école informatique, portfolio, formation informatique, php, html, js, jquery"/>
    <!-- css -->
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="node_modules/spinkit/spinkit.min.css">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/hover.css/css/hover-min.css">
    <link rel="stylesheet" href="medias/css/styles.css">

</head>
<body>
<div class="loader">
    <div class="sk-wave">
        <div class="sk-wave-rect"></div>
        <div class="sk-wave-rect"></div>
        <div class="sk-wave-rect"></div>
        <div class="sk-wave-rect"></div>
        <div class="sk-wave-rect"></div>
    </div>
</div>
<div id="content">
    <section id="nav" class="position-fixed w-100 zindex-fixed">
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto text-uppercase">
                    <li class="nav-item">
                        <a class="nav-link text-white text-decoration-none" href="#about-me">à propos de moi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white text-decoration-none" href="#step">parcours</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white text-decoration-none" href="#project">projets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white text-decoration-none" href="#contact">contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>
    <section id="home" class="d-flex align-items-center justify-content-center flex-column">
        <img src="medias/img/perso.png" width="150" alt="">
        <h1 class="mt-1">Pierre HÉRAUD</h1>
        <p>{$développeur_web}</p>
        <div class="social-icon flex-column mb-4">
            <a href="https://www.linkedin.com/in/pierre-heraud-1b128112b/" target="_blank"><i
                        class="fab fa-linkedin"></i></a>
            <a href="https://github.com/akileine13" target="_blank"><i class="fab fa-github"></i></a>
            <a href="https://gitlab.com/Akileine13" target="_blank"><i class="fab fa-gitlab"></i></a>
            <a href="mailto:pierre.heraud@supinfo.com"><i class="fas fa-envelope"></i></a>
        </div>
        <a href="#about-me" class="btn btn-danger hvr-push"><i class="fas fa-arrow-down"></i></a>
    </section>
    <div class="spacer" data-height="60" style="height: 60px;"></div>
    <section id="about-me">
        <div class="container">
            <h2>À propos de moi</h2>
            <div class="spacer" data-height="60" style="height: 60px;"></div>
            <div class="row">
                <div class="col-md-3 d-flex align-items-center justify-content-center">
                    <img src="medias/img/perso.png" width="200" alt="">
                </div>
                <div class="col-md-9">
                    <div class="bg-purple p-3 rounded">
                        <p>Je m'appelle Pierre HÉRAUD, âgé de 25 ans, je suis actuellement en quatrième année d'étude en
                            informatique à <a href="https://www.keyce.fr/" target="_blank">Keyce Academy</a>, sur le campus de Montpellier et en alternance en tant que
                            Développeur WEB à <a href="https://suncha.fr" target="_blank">Suncha</a>.
                            <br><br>
                            En 2013, j'ai obtenu un BAC Sciences Technique de l'Ingénieur Développement Durable
                            spécialité Energie Environnement (STI2DEE), puis un BTS Conception et Réalisation de
                            Systèmes Automatiques (CRSA) en 2015.
                            <br><br>
                            L'année suivante, n'ayant pas trouvé de poste disponible dans la maintenance en automatisme,
                            j'ai décidé de me réorienter vers un Master en informatique.
                            <br><br>
                            Ce site a pour but de partager mon parcours scolaire et professionnel, vous y trouverez
                            aussi mes projets réalisés pendant mes années de lycée, BTS, et au sein de SUPINFO.
                            <br><br>
                            <i>Il sera mis à jour, et en amélioration constante au fur et à mesure de mon apprentissage.</i></p>
                        <div class="text-center">
                            <a href="" class="btn btn-danger hvr-push">Télécharger mon CV</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="spacer" data-height="60" style="height: 60px;"></div>
    <section id="step">
        <div class="container">
            <h2>Parcours</h2>
            <div class="spacer" data-height="60" style="height: 60px;"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="bg-purple p-3 rounded h-100">
                        <div class="text-center">
                            <h3>Études</h3>
                            <div class="filet"></div>
                        </div>
                        <div class="spacer" data-height="60" style="height: 60px;"></div>
                        <div class="study">
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2019 - Actuellement </span>
                                <h4 class="title">Master 1 - Manager de projets informatiques</h4>
                                <p><a href="https://www.keyce.fr/" target="_blank">Keyce Academy</a> - Mauguio</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2016 - 2019 </span>
                                <h4 class="title">Licence - Spécialiste en informatique</h4>
                                <p><a href="https://www.supinfo.com/fr/Default.aspx" target="_blank">Supinfo International university</a> - Montpellier</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2013 - 2015 </span>
                                <h4 class="title">Brevet Techinicien Supérieur - Conception et Réalisation de Systèmes Automatiques (CRSA)</h4>
                                <p><a href="http://www.lyc-langevin.ac-aix-marseille.fr/spip/" target="_blank">Lycée Paul Langevin</a> - Martigues</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2010 - 2013 </span>
                                <h4 class="title">Baccalauréat - Sciences Technique de l'Ingénieur Développement Durable spécialité Energie Environnement (STI2DEE)</h4>
                                <p><a href="http://www.lyc-langevin.ac-aix-marseille.fr/spip/" target="_blank">Lycée Paul Langevin</a> - Martigues</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="bg-purple p-3 rounded h-100">
                        <div class="text-center">
                            <h3>Stages/Alternances</h3>
                            <div class="filet"></div>
                        </div>
                        <div class="spacer" data-height="60" style="height: 60px;"></div>
                        <div class="job">
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2018 - Actuellement </span>
                                <h4 class="title">Alternance - Développeur WEB</h4>
                                <p><a href="https://suncha.fr" target="_blank">Suncha</a> - Mauguio</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2018</span>
                                <h4 class="title">Stage - Développeur WEB</h4>
                                <p><a href="https://suncha.fr" target="_blank">Suncha</a> - Mauguio | 4 mois</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2017</span>
                                <h4 class="title">Stage - Développeur WEB</h4>
                                <p><a href="https://www.aqena.fr" target="_blank">Aqena</a> - Martigues | 3 mois</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2014</span>
                                <h4 class="title">Stage - Technicien de maintenance</h4>
                                <p><a href="http://www.ch-martigues.fr/-1.html" target="_blank">Centre Hospitalier</a> - Martigues | 2 mois</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer" data-height="60" style="height: 60px;"></div>
            <div class="bg-purple p-3 rounded">
                <div class="row">
                    <div class="col-md-7">
                        <div class="text-center">
                            <h3>Expériences diverses</h3>
                            <div class="filet"></div>
                        </div>
                        <div class="spacer" data-height="60" style="height: 60px;"></div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2016</span>
                                <h4 class="title">Intérim - Manutention</h4>
                                <p>Duo Industrie - Lansargues | 2 mois</p>
                                <p>Asics - Mauguio | 4 mois</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2015</span>
                                <h4 class="title">Intérim - Manutention</h4>
                                <p>Asics - Mauguio | 3 mois</p>
                                <p>La Poste - Montpellier | 1 mois</p>
                                <p>Fnac - Montpellier | 1 semaine</p>
                                <p>Transmanudem - Lansargues | 1 semaine</p>
                                <p>Maison du monde - Fos sur mer | 2 mois</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <span class="time">2012 & 2013</span>
                                <h4 class="title">Emploi saisonnier - Plongeur</h4>
                                <p>Eurocopter - Marignane | 4 mois</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="text-center">
                            <h3>Divers</h3>
                            <div class="filet"></div>
                        </div>
                        <div class="spacer" data-height="60" style="height: 60px;"></div>
                        <div class="timeline-container">
                            <div class="content">
                                <h5>Loisirs :</h5>
                                <p>Développement web, football, voyages (Canada, Slovénie, Royaume-Uni...)</p>
                            </div>
                        </div>
                        <div class="timeline-container">
                            <div class="content">
                                <h5>Atouts :</h5>
                                <p>Curieux, rigoureux, dynamique</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="spacer" data-height="60" style="height: 60px;"></div>
    <section id="project">
        <div class="container">
            <h2>Projets</h2>
            <div class="spacer" data-height="60" style="height: 60px;"></div>
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>
    <div class="spacer" data-height="60" style="height: 60px;"></div>
    <section id="contact">
        <div class="container">
            <h2>Contact</h2>
            <div class="spacer" data-height="60" style="height: 60px;"></div>
        </div>
    </section>
</div>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="medias/js/script.js"></script>
</body>
</html>