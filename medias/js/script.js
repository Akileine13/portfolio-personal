$('#content').hide();
$(window).on('load', function() {
    $('.loader').hide();
    $('#content').show();
});

$(function () {
    //Scroll smooth
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 1000);
    });

});